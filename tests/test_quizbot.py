from typing import Any
from typing import Dict
from typing import List
from typing import Tuple

import pytest
from django.contrib.auth.models import User
from django.test import Client
from django.urls import reverse
from django.utils.text import slugify

from quizbot.models import Answer
from quizbot.models import Quiz, Question


def login(client: Client, django_user_model: User) -> User:
    """ User Authentication """
    user = django_user_model.objects.create_user(username='user1', password='bar')  # <-- Create User
    client.login(username='user1', password='bar')  # <-- Login User
    return user


@pytest.mark.django_db
def test_home_page(client: Client, django_user_model: User):
    """ Check Home response without Authenticate """

    # Run function
    resp = client.get('/')

    # Test output
    assert resp.status_code == 200


def _quiz_question_counts(quizzes: List[Dict[str, Any]],
                          ) -> List[Tuple[
    str,  # Quiz topic name
    int,  # Count of unanswered questions left.
]]:
    return [
        (quiz['topic'], quiz['count_questions'])
        for quiz in quizzes
    ]


def _create_quiz(quiz: Quiz, questions: List[Tuple[Question, List[Answer],]],
                 ) -> Tuple[Quiz, List[Tuple[Question, List[Answer],]],]:
    if not quiz.slug:
        quiz.slug = slugify(quiz.topic)
    quiz.save()

    for q, answers in questions:
        q.quiz = quiz
        q.is_created = True
        q.save()

        for answer in answers:
            answer.question = q
            answer.save()

    return quiz, questions


@pytest.mark.django_db
def test_quiz_status(client: Client, django_user_model: User):
    """
    Check Home response with User Authenticate
    Show Quizzes in Home page
    Take Quiz
    Show Question
    Take Answer
    Go back into Home page & then show how Questions is left in every Quiz
    """
    login(client,django_user_model)

    quiz1, quiz1_questions = _create_quiz(
        Quiz(topic='Python', is_active=True),
        [
            (Question(question='What is python?'), [
                Answer(answer_text='Language', is_correct=True),
                Answer(answer_text='snake', is_correct=False),
            ]),
            (Question(question='What is BDFL?'), [
                Answer(answer_text='Guido', is_correct=True),
                Answer(answer_text='Dictator', is_correct=False),
            ]),
        ],
    )

    quiz2, quiz2_questions = _create_quiz(
        Quiz(topic='Julia', is_active=True),
        [
            (Question(question='What is julia?'), [
                Answer(answer_text='Language', is_correct=True),
                Answer(answer_text='name', is_correct=False),
            ]),
            (Question(question='What is JIT?'), [
                Answer(answer_text='Compiler', is_correct=True),
                Answer(answer_text='A tool', is_correct=False),
            ]),
        ],
    )

    # Visit list of Quizzes
    resp = client.get(reverse('home'))
    assert resp.status_code == 200
    quizzes: List[Dict[str, Any]] = list(resp.context['quizzes'])
    assert _quiz_question_counts(quizzes) == [
        (quiz1.topic, 2),
        (quiz2.topic, 2),
    ]

    # Pick a Quiz1 from list.
    resp = client.get(reverse('quiz', args=[quiz1.slug]))
    assert resp.status_code == 200

    # Submit an answer.
    question: Question = resp.context['question']
    resp = client.post(reverse('quiz', args=[quiz1.slug]), {
        'question_pk': question.pk,
        'answer_pk': question.answers.get(is_correct=True).pk,
    })
    assert resp.status_code == 302

    # Visit list of Quizzes again
    resp = client.get(reverse('home'))
    assert resp.status_code == 200
    quizzes: List[Dict[str, Any]] = list(resp.context['quizzes'])
    assert _quiz_question_counts(quizzes) == [
        ('Python', 1),
        ('Julia', 2),
    ]

    # Pick a Quiz2 from list.
    resp = client.get(reverse('quiz', args=[quiz2.slug]))
    assert resp.status_code == 200

    # Submit an answer.
    question: Question = resp.context['question']
    resp = client.post(reverse('quiz', args=[quiz2.slug]), {
        'question_pk': question.pk,
        'answer_pk': question.answers.get(is_correct=True).pk,
    })
    assert resp.status_code == 302

    # Visit list of Quizzes again
    resp = client.get(reverse('home'))
    assert resp.status_code == 200
    quizzes: List[Dict[str, Any]] = list(resp.context['quizzes'])
    assert _quiz_question_counts(quizzes) == [
        ('Python', 1),
        ('Julia', 1),
    ]
