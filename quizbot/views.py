from django.shortcuts import render, redirect, get_object_or_404

from quizbot.forms import QuizForm
from quizbot.models import Answer
from quizbot.models import Question
from quizbot.models import Quiz, QuizTaker, UserAnswer
from quizbot.services import get_new_question, count_questions


def home_view(request):
    """ Home page show Quizzes """
    quizzes = Quiz.objects.filter(is_active=True)
    if request.user.is_authenticated:
        context = {
            'quizzes': [
                {
                    'topic': quiz.topic,
                    'count_questions': count_questions(quiz, request.user),
                    'description': quiz.description,
                    'url': quiz.get_absolute_url(),
                }
                for quiz in quizzes
            ],
        }
        return render(request, 'home_user.html', context)
    else:
        return render(request, 'home.html')


def quiz_view(request, slug: str):
    """ Quiz: Show questions & answers """
    quiz: Quiz = get_object_or_404(Quiz, slug=slug)
    if request.method == 'POST':
        question_pk = request.POST.get('question_pk')
        question: Question = get_object_or_404(Question, pk=question_pk)
        answer_pk = request.POST.get('answer_pk')
        answer: Answer = get_object_or_404(question.answers, pk=answer_pk)
        quiz_taker, created = QuizTaker.objects.get_or_create(
            user=request.user,
            quiz=quiz,
        )
        (
            UserAnswer.objects.
            filter(quiz_user=quiz_taker, answer_user=answer).
            delete()
        )
        UserAnswer.objects.create(
            quiz_user=quiz_taker,
            question_user=question,
            answer_user=answer,
            is_correct=answer.is_correct,
            marks_obtained=question.question_score,
        )
        question_result = quiz_taker.quiztaker.select_related(
            'question_user').get(question_user__pk=question_pk)

        quiz_taker.update_score()

        return redirect(question_result)
    else:
        question = get_new_question(request.user, slug)
        try:
            quiz_user = QuizTaker.objects.get(
                user=request.user,
                quiz=quiz,
            )
        except QuizTaker.DoesNotExist:
            quiz_user = None

        context = {
            'question': question,
            'user': request.user,
            'score': quiz_user.score if quiz_user else 0,
        }
        return render(request, 'quiz.html', context)


def result_view(request, slug: str, pk: int):
    """ UserAnswer results """
    question_answer = get_object_or_404(
        UserAnswer, pk=pk, question_user__quiz__slug=slug)
    context = {
        'question_answer': question_answer,
    }
    return render(request, 'result.html', context)


def leaderboard(request):
    """ Quiz Leader Board """
    quiz_takers = QuizTaker.objects.order_by('-score', '-quiz')[:100]
    total_count = quiz_takers.count()
    context = {
        'quiz_takers': quiz_takers,
        'total_count': total_count,
    }
    return render(request, 'leaderboard.html', context)


def create_quiz_view(request):
    form = QuizForm(request.POST or None)
    if form.is_valid():
        form.save()
    context = {
        'form': form
    }
    return render(request, 'create_quiz.html', context)


def error_404(request, *args, **kwargs):
    return redirect('home')


def error_500(request, *args, **kwargs):
    return redirect('home')
