from django.contrib import admin

from quizbot import models


@admin.register(models.Quiz)
class QuizAdmin(admin.ModelAdmin):
    """ from Quiz model """
    readonly_fields = ['timestamp']
    list_display = ['topic', 'timestamp', 'is_active']
    list_editable = ['is_active']
    exclude = ['slug']


class AnswerInlineModel(admin.TabularInline):
    """ from Answer model redirect TabularInline """
    model = models.Answer
    field = ['answer_text', 'is_correct']
    max_num = models.Answer.MAX_CHOICES_COUNT
    min_num = models.Answer.MIN_CHOICES_COUNT


@admin.register(models.Question)
class QuestionAdmin(admin.ModelAdmin):
    """ There two models together Question & Answer using inlines """
    fields = ['question', 'quiz', 'question_score', 'is_created']
    list_display = ['question', 'quiz', 'date_created', 'is_created']
    list_editable = ['is_created']
    inlines = (AnswerInlineModel,)


@admin.register(models.QuizTaker)
class QuizTakerAdmin(admin.ModelAdmin):
    """ from QuizTaker model """
    list_display = ['user', 'quiz', 'score']


@admin.register(models.UserAnswer)
class UserAnswerAdmin(admin.ModelAdmin):
    """ from UserAnswer model """
    list_display = [
        "question_user",
        'quiz_user',
        'answer_user',
        'is_correct',
        'marks_obtained'
    ]
