"""quizbot URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path

from quizbot.users.views import SignUpView, ActivateAccount
from quizbot.views import home_view, quiz_view, result_view, \
    leaderboard, error_404, error_500, create_quiz_view

urlpatterns = [
    # VIEWS #
    path('', home_view, name='home'),
    path('quiz/<str:slug>/', quiz_view, name='quiz'),
    path('quiz/<str:slug>/<int:pk>/', result_view, name='result'),
    path('leaderboard/', leaderboard, name='leaderboard'),
    path('createquiz/', create_quiz_view, name='create_quiz_view'),

    # USERS_VIEWS #
    path('register/', SignUpView.as_view(), name="register"),
    path('activate/<uidb64>/<token>/',
         ActivateAccount.as_view(), name='activate'),
    path('login/', auth_views.LoginView.as_view(
        template_name='users/login.html'), name="login"),
    path('logout/', auth_views.LogoutView.as_view(
        template_name='users/logout.html'), name="logout"),

    # ADMIN #
    path('admin/', admin.site.urls),
]

handler404 = error_404
handler500 = error_500
