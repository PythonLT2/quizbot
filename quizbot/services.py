import random
from typing import Any, Union

from django.contrib.auth.base_user import AbstractBaseUser

from quizbot.models import Question, Quiz, UserAnswer


# Quiz services #
def count_questions(quiz: Quiz, user: Union[AbstractBaseUser]) -> int:
    """ Count how many questions is left """
    questions = quiz.questions.filter(is_created=True).count()
    taken_questions = (
        UserAnswer.objects.
        filter(question_user__quiz=quiz, quiz_user__user=user).
        count()
    )
    left_questions = questions - taken_questions
    return left_questions


# QuizTaker services #
def get_new_question(user: Union[AbstractBaseUser], slug: str) -> Any:
    """ Shows every question that is left """
    remaining = (
        Question.objects.
        exclude(answers__useranswer__quiz_user__user=user).
        filter(quiz__slug=slug)
    )
    if remaining.exists():
        return random.choice(remaining)
