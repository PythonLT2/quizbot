from __future__ import annotations

from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from django.db import models
from django.db.models.query import QuerySet
from django.template.defaultfilters import slugify
from django.urls import reverse


class Quiz(models.Model):
    """ Quiz model """
    topic = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    slug = models.SlugField(default=slugify(topic), unique=True)
    is_active = models.BooleanField(default=False, null=False)
    timestamp = models.DateTimeField(auto_now_add=True)

    questions: QuerySet[Question]

    class Meta:
        """ Show correct plural name """
        verbose_name_plural = 'Quizzes'

    def __str__(self):
        return self.topic

    def save(self, *args, **kwargs):
        self.slug = slugify(self.topic)
        super(Quiz, self).save(*args, **kwargs)

    def get_absolute_url(self):
        """ Get Quiz topic questions """
        return reverse('quiz', kwargs={'slug': self.slug})


class Question(models.Model):
    """ Question model """
    quiz = models.ForeignKey(
        Quiz,
        on_delete=models.CASCADE,
        related_name='questions',
        verbose_name='Quiz topic',
    )
    question = RichTextField(blank=False, null=False)
    date_created = models.DateTimeField(auto_now_add=True)
    is_created = models.BooleanField(default=True, null=False)
    question_score = models.DecimalField(default=1, decimal_places=0,
                                         max_digits=6, )

    answers: QuerySet[Answer]

    def __str__(self):
        return self.question


class Answer(models.Model):
    """ Answer model """
    MAX_CHOICES_COUNT = 5
    MIN_CHOICES_COUNT = 2

    question = models.ForeignKey(Question, on_delete=models.CASCADE,
                                 related_name='answers')
    answer_text = models.CharField(max_length=100)
    is_correct = models.BooleanField(default=False, null=False)

    def __str__(self):
        return self.answer_text


class QuizTaker(models.Model):
    """ Quiz Taker model """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, null=True)
    score = models.DecimalField(default=0, decimal_places=0, max_digits=10)

    class Meta:
        unique_together = [
            ('user', 'quiz'),
        ]

    def __str__(self):
        return str(self.user)

    def update_score(self):
        """ Update every QuizTaker correct answer """
        score_sum = self.quiztaker.filter(is_correct=True).aggregate(
            models.Sum('marks_obtained'))['marks_obtained__sum']
        self.score = score_sum or 0
        self.save()


class UserAnswer(models.Model):
    """ User Answers model """
    quiz_user = models.ForeignKey(QuizTaker, on_delete=models.CASCADE,
                                  related_name='quiztaker')
    question_user = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer_user = models.ForeignKey(Answer, on_delete=models.CASCADE,
                                    null=True)
    is_correct = models.BooleanField(default=False, null=False)
    marks_obtained = models.DecimalField(default=0, decimal_places=0,
                                         max_digits=6)

    def get_absolute_url(self):
        """ Get Quiz slug & UserAnswer question pk """
        return reverse('result', kwargs={
            'slug': self.question_user.quiz.slug,
            'pk': self.pk})
