from django.forms import ModelForm

from quizbot.models import Quiz, Question, Answer


class QuizForm(ModelForm):
    class Meta:
        model = Quiz
        fields = ['topic', 'description', 'is_active']


class QuestionForm(ModelForm):
    class Meta:
        model = Question
        fields = ['quiz', 'question', 'question_score']


class AnswerForm(ModelForm):
    question = QuestionForm

    class Meta:
        model = Answer
        fields = ['answer_text', 'is_correct']
