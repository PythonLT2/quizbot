| **Instruction to start**

* Install Poetry.
* https://python-poetry.org/docs/#installation
* poetry --version

| Configure Poetry to store venvs in project directory.

* poetry config virtualenvs.in-project true
* poetry config --list

| Check if you are on the right directory

| Clone **QuizBot:**

* git clone git@gitlab.com:PythonLT2/quizbot.git
* Check if you are on the right directory where is pyproject.toml file
* poetry install

| Activate poetry virtual environment:

* poetry shell

| Create database table:

* python manage.py migrate
* python manage.py makemigrations

| To runserver type into terminal:

* python manage.py runserver

| Deploying app with HEROKU

* https://www.heroku.com/

| Python Poetry BuildPack with HEROKU

* https://elements.heroku.com/buildpacks/moneymeets/python-poetry-buildpack

| **Authors:**

* Tadas Savickas
* Dalius Deveikis


